mlocate (0.26-4) unstable; urgency=medium

  * The mlocate package now ships a systemd timer unit for updatedb.
    On systems running systemd, the cron job script will exit early,
    expecting the systemd unit to fire instead. The systemd unit and
    timer have the following advantages and disadvantages compared to
    running via cron:

        + The updatedb process runs in a restricted process context
          (see the descriptions of PrivateTmp, PrivateNetwork,
          PrivateDevices and ProtectSystem in systemd.exec(5))

        + systemd will automatically splay the timer around a 24h 
          period, taking into consideration other scheduled jobs, to
          mitigate thundering herd and prevent multiple machines in
          a cluster all running the job at the same time

        - the systemd timer does not (yet) run the process under 
          "nocache" when available.

 -- Jonathan Dowland <jmtd@debian.org>  Mon, 09 Nov 2020 11:03:28 +0000
